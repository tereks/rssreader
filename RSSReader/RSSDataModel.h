//
//  RSSDataModel.h
//  RSSReader
//
//  Created by Sergey Kim on 23.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RSSDataModelDelegate <NSObject>

- (void) reloadData;

@end

@interface RSSDataModel : NSObject

@property (nonatomic, strong) NSMutableArray * feeds;
@property (nonatomic, weak) id<RSSDataModelDelegate> delegate;

- (instancetype) initWithURL:(NSURL*)url;
- (void) startDownloading;

@end
