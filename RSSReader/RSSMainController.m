//
//  RSSMainController.m
//  RSSReader
//
//  Created by Sergey Kim on 23.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "RSSMainController.h"
#import "RSSDataModel.h"
#import "WebViewController.h"

#import "SVProgressHUD.h"

static NSString * const rssUrl = @"https://news.mail.ru/rss/politics/91/";
static NSString * const rssCellIdentifier = @"rssCell";

@interface RSSMainController () <RSSDataModelDelegate>

@property (nonatomic, strong) RSSDataModel * dataModel;
@property (nonatomic, strong) NSString * urlString;

@end

@implementation RSSMainController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"";
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    NSURL *url = [NSURL URLWithString:rssUrl];    
    self.dataModel = [[RSSDataModel alloc] initWithURL:url];
    self.dataModel.delegate = self;
    [SVProgressHUD show];
    
    [self.dataModel startDownloading];    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataModel.feeds.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:rssCellIdentifier forIndexPath:indexPath];
    cell.textLabel.text = [[self.dataModel.feeds objectAtIndex:indexPath.row] objectForKey: @"title"];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.font = [UIFont systemFontOfSize:13];
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.urlString = [[self.dataModel.feeds objectAtIndex:indexPath.row] objectForKey: @"link"];
    [self performSegueWithIdentifier:@"showWeb" sender:self];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [[segue identifier] isEqualToString:@"showWeb"] && self.urlString.length > 0 ) {
        WebViewController * webController = (WebViewController*)segue.destinationViewController;
        webController.url = [NSURL URLWithString:self.urlString];
    }
}

- (void) reloadData {
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
        [self.tableView reloadData];
    });
}

@end
