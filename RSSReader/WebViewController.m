//
//  WebViewController.m
//  RSSReader
//
//  Created by Sergey Kim on 23.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "WebViewController.h"
#import "SVProgressHUD.h"

@interface WebViewController () <UIWebViewDelegate>

@property (nonatomic, weak) IBOutlet UIWebView * webView;

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"";
    
    self.webView.delegate = self;
    
    [SVProgressHUD show];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:self.url];
    [_webView loadRequest:urlRequest];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}

@end
