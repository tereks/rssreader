//
//  RSSDataModel.m
//  RSSReader
//
//  Created by Sergey Kim on 23.03.16.
//  Copyright © 2016 Sergey Kim. All rights reserved.
//

#import "RSSDataModel.h"

@interface RSSDataModel () <NSXMLParserDelegate>

@property (nonatomic, strong) NSXMLParser *parser;
@property (nonatomic, strong) NSMutableDictionary *item;
@property (nonatomic, strong) NSMutableString *title;
@property (nonatomic, strong) NSMutableString *link;
@property (nonatomic, strong) NSString *element;

@end

@implementation RSSDataModel

- (id) init {
    self = [super init];
    if ( self ) {
        [self initialize];
    }
    return self;
}

- (instancetype) initWithURL:(NSURL*)url {
    self = [super init];
    if ( self ) {
        [self initialize];
        
        self.parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
        [self.parser setDelegate:self];
        [self.parser setShouldResolveExternalEntities:NO];
    }
    return self;
}

- (void) initialize {
    self.feeds = [NSMutableArray new];
}

- (void) startDownloading {
    [self.parser parse];
}

#pragma MARK - NSXMLParserDelegate
- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    self.element = elementName;
    
    if ([self.element isEqualToString:@"item"]) {
        
        self.item  = [[NSMutableDictionary alloc] init];
        self.title = [[NSMutableString alloc] init];
        self.link  = [[NSMutableString alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"item"]) {
        
        [self.item setObject:self.title forKey:@"title"];
        [self.item setObject:self.link forKey:@"link"];
        
        [self.feeds addObject:[self.item copy]];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([self.element isEqualToString:@"title"]) {
        [self.title appendString:string];
    } else if ([self.element isEqualToString:@"link"]) {
        NSString *trimmedLink = [[string componentsSeparatedByCharactersInSet:
                                  [NSCharacterSet whitespaceAndNewlineCharacterSet]] componentsJoinedByString:@""];
        [self.link appendString:trimmedLink];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    if ( [self.delegate respondsToSelector:@selector(reloadData)] ) {
        [self.delegate reloadData];
    }    
}

@end
